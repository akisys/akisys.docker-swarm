import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_hosts_file(host):
    f = host.file('/etc/hosts')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


def test_docker_status(host):
    docker = host.service('docker')

    assert docker.is_running
    assert docker.is_enabled


def test_docker_swarm_nodes(host):
    with host.sudo():
        result = host.check_output('docker info | grep "Swarm: active"')
    assert result == 'Swarm: active'
